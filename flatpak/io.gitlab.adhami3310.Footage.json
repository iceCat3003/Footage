{
    "id": "io.gitlab.adhami3310.Footage",
    "runtime": "org.gnome.Platform",
    "runtime-version": "44",
    "sdk": "org.gnome.Sdk",
    "sdk-extensions": [
        "org.freedesktop.Sdk.Extension.rust-stable"
    ],
    "cleanup-commands": [
        "mkdir -p ${FLATPAK_DEST}/lib/ffmpeg"
    ],
    "command": "footage",
    "finish-args": [
        "--share=ipc",
        "--socket=fallback-x11",
        "--device=dri",
        "--socket=wayland",
        "--socket=pulseaudio"
    ],
    "build-options": {
        "append-path": "/usr/lib/sdk/rust-stable/bin",
        "env": {
            "GST_PLUGIN_PATH": "/app/lib/codecs/lib/gstreamer-1.0",
            "GST_PLUGIN_SYSTEM_PATH": "/app/lib/gstreamer-1.0/",
            "GST_ENCODING_TARGET_PATH": "/app/share/gstreamer-1.0/encoding-profiles/:/app/share/pitivi/encoding-profiles/",
            "GST_VAAPI_ALL_DRIVERS": "1",
            "CARGO_HOME": "/run/build/footage/cargo"
        },
        "append-ld-library-path": "/app/lib/codecs/lib",
        "ldflags": "-L/app/lib/codecs/lib"
    },
    "cleanup": [
        "/include",
        "/lib/*.a",
        "/lib/*.la",
        "/lib/gstreamer-1.0/include",
        "/lib/pkgconfig",
        "/share/man"
    ],
    "modules": [
        {
            "name": "blueprint-compiler",
            "buildsystem": "meson",
            "cleanup": [
                "*"
            ],
            "sources": [
                {
                    "type": "git",
                    "url": "https://gitlab.gnome.org/jwestman/blueprint-compiler.git",
                    "tag": "v0.8.1",
                    "commit": "aa7679618e864748f4f4d8f15283906e712752fe"
                }
            ]
        },
        {
            "name": "liba52",
            "config-opts": [ "--enable-shared", "--disable-static" ],
            "rm-configure": true,
            "cleanup": [ "/bin/*a52*" ],
            "sources": [
                {
                    "type": "archive",
                    "url": "http://liba52.sourceforge.net/files/a52dec-0.7.4.tar.gz",
                    "sha256": "a21d724ab3b3933330194353687df82c475b5dfb997513eef4c25de6c865ec33"
                },
                {
                    "type": "patch",
                    "path": "patches/a52dec-0.7.4-rpath64.patch"
                },
                {
                    "type": "patch",
                    "path": "patches/a52dec-configure-optflags.patch"
                },
                {
                    "type": "patch",
                    "path": "patches/liba52-silence.patch"
                },
                {
                    "type": "patch",
                    "path": "patches/liba52-prefer-pic.patch"
                },
                {
                    "type":"script",
                    "commands":[
                        "autoreconf -fiv"
                    ],
                    "dest-filename":"autogen.sh"
                }
            ]
        },
        {
            "name": "libmpeg2",
            "config-opts": [ "--enable-shared", "--disable-static" ],
            "rm-configure": true,
            "cleanup": [ "/bin/*mpeg2*" ],
            "sources": [
                {
                    "type": "archive",
                    "url": "http://libmpeg2.sourceforge.net/files/libmpeg2-0.5.1.tar.gz",
                    "sha256": "dee22e893cb5fc2b2b6ebd60b88478ab8556cb3b93f9a0d7ce8f3b61851871d4"
                },
                {
                    "type": "patch",
                    "path": "patches/libmpeg2-inline.patch"
                },
                {
                    "type":"script",
                    "commands":[
                        "autoreconf -fiv"
                    ],
                    "dest-filename":"autogen.sh"
                }
            ]
        },        
        {
            "name": "x265",
            "buildsystem": "cmake-ninja",
            "subdir": "source",
            "sources": [
                {
                    "type": "archive",
                    "url": "https://bitbucket.org/multicoreware/x265_git/downloads/x265_3.5.tar.gz",
                    "sha256": "e70a3335cacacbba0b3a20ec6fecd6783932288ebc8163ad74bcc9606477cae8",
                    "x-checker-data": {
                        "type": "html",
                        "url": "https://bitbucket.org/multicoreware/x265_git/raw/stable/x265Version.txt",
                        "version-pattern": "releasetag: ([\\d\\.-]*)$",
                        "url-template": "https://bitbucket.org/multicoreware/x265_git/downloads/x265_$version.tar.gz"
                    }
                }
            ]
        },
        {
            "name": "x264",
            "config-opts": [
                "--enable-shared",
                "--enable-static",
                "--enable-pic",
                "--disable-lavf"
            ],
            "sources": [
                {
                    "type": "git",
                    "url": "https://code.videolan.org/videolan/x264.git",
                    "//": "Just get the latest from https://code.videolan.org/videolan/x264/-/commits/stable/ they don't do tags/snapshots/releases anymore",
                    "commit": "baee400fa9ced6f5481a728138fed6e867b0ff7f"
                }
            ]
        },
        {
            "name": "fdkaac",
            "config-opts": [
                "--disable-static"
            ],
            "sources": [
                {
                    "type": "git",
                    "url": "https://github.com/mstorsjo/fdk-aac.git",
                    "tag": "v0.1.6",
                    "commit": "a30bfced6b6d6d976c728552d247cb30dd86e238"
                }
            ]
        },
        {
            "name": "vo-aacenc",
            "sources": [
                {
                    "type": "archive",
                    "url": "https://downloads.sourceforge.net/opencore-amr/vo-aacenc/vo-aacenc-0.1.3.tar.gz",
                    "mirror-urls": [
                        "http://ftp.debian.org/debian/pool/main/v/vo-aacenc/vo-aacenc_0.1.3.orig.tar.gz"
                    ],
                    "sha256": "e51a7477a359f18df7c4f82d195dab4e14e7414cbd48cf79cc195fc446850f36"
                }
            ]
        },
        {
            "name": "svt-av1",
            "buildsystem": "cmake-ninja",
            "builddir": true,
            "config-opts": [
                "-DCMAKE_BUILD_TYPE=Release",
                "-DBUILD_SHARED_LIBS=ON",
                "-DBUILD_APPS=OFF",
                "-DBUILD_DEC=ON",
                "-DBUILD_ENC=ON",
                "-DBUILD_TESTING=OFF"
            ],
            "cleanup": [
                "/include",
                "/lib/pkgconfig"
            ],
            "sources": [
                {
                    "type": "git",
                    "url": "https://gitlab.com/AOMediaCodec/SVT-AV1",
                    "commit": "ea296ef350714fb6f105b420fb0bc321d9997ffd"
                }
            ]
        },
        {
            "name": "gstreamer-svt-av1",
            "buildsystem": "simple",
            "build-commands": [
                "cd gstreamer-plugin && meson -Dprefix=/app build",
                "cd gstreamer-plugin && ninja -C build install"
            ],
            "sources": [
                {
                    "type": "archive",
                    "url": "https://gitlab.com/AOMediaCodec/SVT-AV1/-/archive/master/SVT-AV1-master.tar.gz?path=gstreamer-plugin",
                    "sha256": "19a9f37baec61b805964a42e0123cd09838a27875f439d0336418fb1dbe7f524"
                }
            ]
        },
        {
            "name": "gstreamer",
            "buildsystem": "meson",
            "builddir": true,
            "config-opts": [
                "--libdir=lib",
                "--wrap-mode=nodownload",
                "-Dgpl=enabled",
                "-Dgood=enabled",
                "-Dbase=enabled",
                "-Dugly=enabled",
                "-Dvaapi=enabled",
                "-Ddoc=disabled",
                "-Dgst-examples=disabled",
                "-Dpython=disabled"
            ],
            "sources": [
                {
                    "type": "git",
                    "tag": "1.22.3",
                    "url": "https://gitlab.freedesktop.org/gstreamer/gstreamer.git",
                    "commit": "ecd471f5ea4645102b206a43d863f0f0fe7d04ec",
                    "disable-submodules": false
                },
                {
                    "type": "git",
                    "url": "https://gitlab.freedesktop.org/gstreamer/orc.git",
                    "commit": "887392103956a8733a750c4442e2664d5c26527d",
                    "dest": "subprojects/orc"
                }
            ]
        },
        {
            "name": "cargo-c",
            "buildsystem": "simple",
            "build-commands": [
                "mkdir .cargo",
                "echo -e '[source.crates-io]\nreplace-with = \"vendored-sources\"\n\n[source.vendored-sources]\ndirectory = \"cargo/vendor\"' >> .cargo/config",
                "cat .cargo/config",
                "cargo --offline fetch --manifest-path Cargo.toml --verbose",
                "cargo install --path . --root /app"
            ],
            "sources": [
                {
                    "type": "git",
                    "url": "https://github.com/lu-zero/cargo-c",
                    "tag": "v0.9.20",
                    "sha256": "ddf235c6d039d7e75f211f62da16fa5baa2bb2ad"
                },
                "cargo-c-sources.json"
            ],
            "cleanup": [
                "*"
            ]
        },
        {
            "name": "gst-plugins-rs",
            "buildsystem": "simple",
            "sources": [
                {
                    "type": "archive",
                    "url": "https://gitlab.com/adhami3310/Footage/-/raw/615ef6e9c600da451ead6113cda06cc0033901b9/flatpak/gst-plugins-rs.tar.xz?inline=false",
                    "sha256": "60d8d83f3228f282c2d93030ff583a7eff0fd7423aba9907ac8d579f89505c20"
                }
            ],
            "build-commands": [
                "cargo --offline fetch --manifest-path Cargo.toml --verbose",
                "cargo cinstall -p gst-plugin-gtk4 --prefix=/app",
                "cargo cinstall -p gst-plugin-gif --prefix=/app"
            ]
        },
        {
            "name": "footage",
            "builddir": true,
            "buildsystem": "meson",
            "config-opts": [
                "--buildtype=release",
                "-Dprofile=development"
            ],
            "sources": [
                {
                    "type": "dir",
                    "path": "."
                }
            ]
        }
    ]
}