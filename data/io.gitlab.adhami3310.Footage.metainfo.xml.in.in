<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
    <id>@app-id@</id>

    <name>Footage</name>
    <developer_name>Khaleel Al-Adhami</developer_name>
    <summary>Polish your videos</summary>

    <metadata_license>CC-BY-SA-4.0</metadata_license>
    <project_license>GPL-3.0-only</project_license>

    <description>
        <p>
            Trim, flip, rotate and crop individual clips. Footage is a useful tool for quickly editing short videos and screencasts. It's also capable of exporting any video into a format of your choice.
        </p>
    </description>

    <translation type="gettext">@gettext-package@</translation>

    <categories>
        <category>System</category>
        <category>Utility</category>
        <category>GTK</category>
        <category>GNOME</category>
    </categories>

    <keywords>
        <keyword>video</keyword>
        <keyword>edit</keyword>
        <keyword>trim</keyword>
        <keyword>crop</keyword>
        <keyword>convert</keyword>
    </keywords>

    <requires>
        <internet>offline-only</internet>
    </requires>

    <recommends>
        <control>pointing</control>
        <control>keyboard</control>
        <control>touch</control>
    </recommends>

    <screenshots>
        <screenshot>
            <image>https://gitlab.com/adhami3310/Footage/-/raw/main/data/resources/screenshots/0.png</image>
            <caption>Editing screen with default options</caption>
        </screenshot>
    </screenshots>

    <launchable type="desktop-id">@app-id@.desktop</launchable>

    <url type="homepage">https://gitlab.com/adhami3310/Footage</url>
    <url type="bugtracker">https://gitlab.com/adhami3310/Footage/-/issues</url>
    <url type="help">https://gitlab.com/adhami3310/Footage/-/issues</url>
    <url type="vcs-browser">https://gitlab.com/adhami3310/Footage</url>
    <url type="contribute">https://gitlab.com/adhami3310/Footage</url>
    <url type="contact">https://matrix.to/#/@adhami:matrix.org</url>

    <update_contact>khaleel.aladhami@gmail.com</update_contact>

    <content_rating type="oars-1.1" />

    <releases>
        <release version="1.1" date="2023-06-16">
            <description>
                <p>This minor release introduces a couple of bug fixes and enhances keyboard accessibility.</p>
            </description>
        </release>
        <release version="1.0" date="2023-06-13">
            <description>
                <p>Initial version.</p>
            </description>
        </release>
    </releases>
</component>
